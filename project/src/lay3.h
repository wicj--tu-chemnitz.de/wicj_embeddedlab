#ifndef L3_H
#define L3_H
    
    #include<stdlib.h>
    #include "crc.h"
    
    /** The unique node adress. */
    #define SELF 5

    /** Declaration of the maximum amount of messages in the message queue #msgQueue.*/ 
    #define MAXQUEUESIZE 7
    
    /** A function handling a successful broadcast.
    * At the moment it simply prints "Broadcast successfull".
    */
    void broadcast_successful();
    
    /** A function to determine how to proceed in layer 3 according to the specification.
    * There are four possible #actions to perform. If #dest equals 0, the #receivedFrame is a broadcast message. 
    * In case the node is the source of the broadcast the function returns 0.
    * If the node is the intended receiver of the #receivedFrame, the function returns 1.
    * If a #receivedFrame sent by the node itself returns, the receiver was not found in the ring. The function then 
    * returns 3. In all other cases the function returns 2.
    *
    * @param[in] dest The destination adress of the received message.
    * @param[in] src The source adress of the received message.
    */
    byte l3_action(byte dest, byte src);

    /** A function to execute layer 3 functionality.
    * The functionality specified in the protocol is executed according to the parameters.
    * @param[in] action A variable indicating which action should be performed by layer 3.
    * If #action equals 0, a broadcast has circulated the network successfully.
    * If #action equals 1, the node is the intended receiver for the received frame.
    * If #action equals 2, the received message should be relayed further.
    * @param[in] frame The received message to process. 
    * Messages processed by l3_exec() are inserted at the beginning of the message queue #msgQueue.
    */
    void l3_exec(byte action, byte* frame);

    /** A function to send the next frame from the message queue #msgQueue.*/
    void l3_send();

    /** A function to insert a USART message at the end of the message queue #msgQueue. */
    void l3_usart_send(byte* msg, byte len);
    
    /* Introducing some functions from lay1+2.c to lay3.c I know it's evil - sorry.*/
    void fillSend (byte* payload, byte size);
    void sendZeroes();
    
    /** A ring buffer to store incoming and outgoing messages. */
    byte** msgQueue;

#endif
#ifndef CRC_H
#define CRC_H

    #include <stdio.h>
    #include <stdint.h>
    #include <stdlib.h>
	
	/**
	* An unsigned char type
	* Named "byte" for better understanding as well as for shorter usage.
	*/
    typedef unsigned char byte; 

    /**
	* A function to compute and return a 32-bit CRC-value 
	* The function is implemented as a left-shift-register.
	* First, the newly declared register is initialized with 0.
	* Byte by byte the input is left-shifted into the register.
	* For each byte the following is performed:
	* As long as the Most Significant Bit (MSB) equals 0, the register is left-shifted bitwise.
	* If the MSB equals 1, a logical XOR is performed with the register and the CRC-key 0x04C11DB7.
	* The procedure continues until the input is fully processed.
	*
	* Nor input or result is reflected. The final XOR-value equals 0.
	*
	* @param[in] input The payload to compute the CRC-value of.
	* @param[in] size The length of the payload.
	*/

    uint32_t crc_comp (byte* input, byte size);

    /**
	* A function to perform a CRC-check
	* The function will initially read in the checksum sent with the input.
	* Afterwards it computes the CRC-value of the payload.
	* Both values will be compared. In the event of success the function returns 1, else it returns 0.
	*
	* @param[in] input The payload consisting of, inter alia, a 32-bit CRC-value and the corresponding payload.
	*/

    byte crc_check(byte* input);
    
#endif
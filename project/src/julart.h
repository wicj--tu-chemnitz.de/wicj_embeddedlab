#ifndef UART_H
#define UART_H
    /** Definition of CPU Frequency. */
    #define F_CPU 12000000UL

    /** Definition of BAUD rate (= transmission rate) in bps. */
    #define BAUD 115200
    
    /** Capacity of the UART Receivebuffer. */
    #define URB_SIZE 32
    
    #include <avr/io.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <util/setbaud.h>
    #include <avr/interrupt.h>
    
    /** A function to initialize the Input-/Output-Stream.
    * Initializes I/O-Stream and index of receivebuffer. Calls <uart_init>"()".
    */
    void my_uart_init(void);

    /** A function to initialize UART functionality.
    * Sets the UART speed according to CPU frequency (#F_CPU) and baudrate (#BAUD).
    * Sets Data Size to 8-bit. Enables "Receive Complete" and "Transmit Complete" registers.
    * Enables UART Receiver Interrupt.
    * Retrieved from: https://appelsiini.net/2011/simple-usart-with-avr-libc/
    */
    void uart_init(void);

    /** A function to write a character to the stream.
    * @param[in] c The character to write to the stream.
    * @param stream I/O stream.
    */
    int uart_putchar(char c, FILE *stream);

    /** A function to read a character from the I/O-stream.
    * @param[in] c The character read from the stream.
    * @param stream I/O stream.
    */
    int uart_getchar(FILE *stream); 

    /** A function to flush the I/O-stream. */
    void jflush();

#endif
#include "crc.h"

//CRC Computation
uint32_t crc_comp (byte* input, byte size)
{

    uint32_t crc_key = 0x04C11DB7;

    uint32_t crc_reg = 0;

    byte input_index = 0;
    
    while(input_index < size){
      	
      	crc_reg ^= ((uint32_t)input[input_index] << 24); 
    	
    	for(byte bit_index = 0; bit_index < 8; bit_index++){

            uint32_t msb = (crc_reg & ((uint32_t)1 << 31)) >> 31;

            crc_reg <<= 1;
	        if(msb == 1) crc_reg = crc_reg ^ crc_key;
	    }
    	input_index++;
	}
    return crc_reg;
}

//CRC Comparison
byte crc_check(byte* input)
{
    uint32_t checksum = 0;
    for(byte i = 1; i < 5; i++){
         checksum += ( (uint32_t)input[i] << (32 - i*8) );
    }

    byte payload[256];

    for(uint32_t i = 6; i < (uint32_t) (input[5]+6); i++){
        payload[i-6] = input[i];
    }

    uint32_t computed = crc_comp(payload, input[5]);

    if(checksum == computed) {
        return 1;
    }
    else {
        return 0;
    }
}

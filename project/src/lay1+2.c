/** \file lay1+2.c 
* A file processing received data according to the specification of layers 1 and 2.
* It includes the two interrupt service routines for transmitting and receiving the clock signal 
* as well as preamble and CRC checks.
*/

#include "julart.h"
#include "crc.h"
#include "lay3.h"

#include <stdio.h>

#include <stdlib.h>
#include <stdint.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>

/** Definition of the maximum size of a RaspNet-Frame in byte. */
#define MAXFRAMESIZE 256


/** A transmit buffer. */
byte send[MAXFRAMESIZE] = {0};

/** A ring buffer for storing received frames temporarily. */
byte frame[MAXFRAMESIZE] = {0}; // 0 - Preambel, 1 to 4 - CRC checksum, 5 - size, 6 to n (n = max. 256) - Payload

/** A flag to differentiate between sending and clock state.
* If flag is set, send data via pin. Else the clock pin is changed.
*/
byte flag = 1;

/** An index variable to indicate the current byte in ring buffer #frame in ISR(PCINT2_vect). */
byte bufferIndex = 0;

/** An index variable to indicate the bytewise progress in transmit buffer. */
byte sendIndex = 0;

/** An index variable to indicate the bitwise progress in transmit buffer. */
byte sendBitIndex = 8;

/** An index variable to indicate the bitwise progress in receive buffer */
byte bitIndex = 7;

/** A ready-flag indicating when a byte can be read from the receive buffer #frame.
* If #readyToReceive is set, the main() function reads a byte from the buffer for 
* further processing and resets it afterwards. 
*/
byte readyToReceive = 0;

/** A ready-flag indicating when a frame is loaded into the send buffer #send entirely.
* If #readyToSend is set the frame is gradually send via pin in ISR(TIMER1_COMPA_vect).
*/
byte readyToSend = 0;

/** Head of the circular buffer #frame. */ 
byte bufferHead = 0;

/** An own implementation of a delay function.
* @param[in] ms Amount of milliseconds to delay further procession.
*/
void delay(int ms)
{
    while(ms>0){
        _delay_ms(1);
        ms--;
    }
    return;
}

/** A function to fill the send buffer.
* The function pads the given payload with information according to the second layer of RaspNET.
* The first byte is the preambel 01111110.\n
* The next four bytes consist of the 32-bit CRC value.\n
* Further, the size of the payload is stored in the fifth byte.
* All following bytes consist of the payload itself.
*
* @param payload An array containing the source and the destination of the message as well as the message itself.
* @param size The corresponding length of the given #payload.
*/
void fillSend (byte* payload, byte size)
{
    send[0] = 0x7E; // = 0b01111110

    uint32_t checksum = crc_comp(payload, size);
    for(byte i = 1; i < 5; i++){
        byte x = 32-i*8;
        send[i] = (byte) ( (checksum & ( (uint32_t)(255) << (x) ) ) >> x);
    }

    send[5] = size;

    for(byte i = 6; i < size+6; i++){
        send[i]= payload[i-6];
    }
    readyToSend = 1;  
}

/** A function filling the send buffer with zeroes only. */
void sendZeroes()
{
    for(int i = 0; i < MAXFRAMESIZE; i++){
        send[i] = 0;
    }
    sendIndex = 5;
    readyToSend = 1;
    return;
}

/* Function declaration for interrupt setups */

/** A function initializing the timer interrupt.
* The timer shall count up to the value of the Output Compare Register (OCR1A).
* When these values match, an interrupt is triggered and the register is cleared.\n\n
* Computation of Output Compare Register:\n
* OCR1A =  [ (clock_speed / Prescaler_value) * Desired_time_in_Seconds ] - 1\n
* The highest value can be FFFF because there are 16 bits available.
* The chosen prescaler value is 256.
*/
void setup_Timer();

/** A function initializing the pin change interrupt.
* Enables listening on pin change mask 2, clears the pin change interrupt flag
* and enabled the corresponding interrupt for pin D4.
*/
void setup_PinC();

/** The main function implementing RASPNet layers 1 and 2.
* The main function initializes UART functionality by calling my_uart_init(). 
* Also it initializes the data direction registers, assigning D-pins to input and B-pins to output
* functionality. Afterwards interrupt functionality is set up.
* The main() function further initializes variables for handling received frames and allocates memory accordingly. 
* Additional memory is allocated for the message queue #msgQueue. \n
* After the initialization and allocation in the main() function, an endless loop starts processing received frames byte by byte.
* A data package consists of the following parts:\n
* Byte 0: Preamble; Bytes 1 - 4: CRC - value; Byte 5: Length; Byte 6: Destination; 
* Byte 7: Source; Bytes 8 - n Payload (where 7 < n <= #MAXFRAMESIZE)  \n
* When the seventh byte was received, the information to this point is handed to l3_action() in layer 3 to determine further action.
* When the reception is completed, the frame is handed to layer 3 for processing with the help of l3_exec().
* If there is no frame in the send buffer #send, the main function furthermore calls l3_send() to add the next message of
* the message queue #msgQueue to #send.
*/
int main(void)
{
    my_uart_init();
    puts("Starting...");

    DDRD = 0x0;
    DDRB = 0x38;
    
    setup_Timer();
    setup_PinC();
    sei();
    
    byte receiveIndex = 0;
    byte* receivedFrame = malloc(MAXFRAMESIZE*sizeof(byte));
    byte sizeToReceive = MAXFRAMESIZE-1;
    
    /* A variable indicating which action should be performed by layer 3.
    * If #action equals 0, a broadcast has circulated the network successfully.
    * If #action equals 1, the node is the intended receiver for the received frame.
    * If #action equals 2, the received message should be relayed further.
    */
    byte action;
    msgQueue = malloc(7*sizeof(byte*));

    while(1){
        ATOMIC_BLOCK(ATOMIC_FORCEON){
            if(readyToReceive){
                if(bufferIndex != 0) receivedFrame[receiveIndex] = frame[bufferIndex-1];
                else receivedFrame[receiveIndex] = frame[MAXFRAMESIZE-1];
                
                if(receiveIndex==0){
                        if(receivedFrame[receiveIndex] == 0x7E) receiveIndex++;
				}                        
                else if(receiveIndex==5){
                        sizeToReceive = receivedFrame[receiveIndex];
                        receiveIndex++;
                }
                else if(receiveIndex==sizeToReceive+5){
                            if(action !=2 && !crc_check(receivedFrame))
                                puts("CRC-Check failed");
                                
                            else{
                                puts("CRC-Check successful.\nReceived: ");
                                for(byte i = 7; i < sizeToReceive + 6; i++){
									if(i==7) printf("%d -> %d", receivedFrame[7], receivedFrame[6]);
									else printf("%c", receivedFrame[i] );
								}
                                puts("");
                                l3_exec(action, receivedFrame);
                            }
                            receiveIndex = 0;
                }
                else if(receiveIndex==7){
                    action = l3_action(receivedFrame[6], receivedFrame[7]);
                    receiveIndex++;
                }
                else{
                    receiveIndex++;
                }
                readyToReceive = 0;
                break;
            }
        }
        if(!readyToSend) l3_send();
    }  
    return 0;
}

/** Interrupt Service Routine using a 16-Bit Timer.
* If #flag is set, data can be sent. The routine checks if a byte is ready to send.
* Then, #sendBitIndex indicates, which bit will be send. The value (0 or 1) is set on pin B5.
* If #flag is 0, the clock signal is sent via pin B4.
* @param TIMER1_COMPA_vect Enables timer interrupts for a 16 bit timer.
*/
 
 ISR(TIMER1_COMPA_vect)
 {
    if(flag){
        if(readyToSend){
            sendBitIndex--;
            byte bit = (send[sendIndex] & (1 << sendBitIndex)) >> sendBitIndex;

            if(bit) {
                PORTB |= (1 << PB5);  //set PB5 in PORTB
            }
            else {
                PORTB &= ~(1 << PB5); //delete PB5 in PORTB
            }

            if(sendBitIndex==0) {
                sendIndex++;
                sendBitIndex = 8;
            }
            if(sendIndex == send[5]+6) {
                sendIndex = 0;
                readyToSend = 0;
            }
        }
        else PORTB &= ~(1 << PB5);
    }
    else    PORTB ^= (1 << PB4);
    flag^=1;
}

/** An interrupt service routine invoked by pin change.
 * @param PCINT2_vect Enables pin change interrupts, regarding only PCINT[16...23]. 
 * We need PCINT20 as it is corresponding pin D4 which is receiving the clock signal.
 */
 
 ISR(PCINT2_vect)
 {    
        frame[bufferIndex] = (frame[bufferIndex] << 1) + ((PIND & (1 << PD5)) >> PD5);
        
        bitIndex--;
      
        if(bitIndex == 255) {
			if(bufferIndex == bufferHead){
				if(frame[bufferIndex] != 0x7E){
					bitIndex=0;
					return;
				}
				else puts("Preambel detected");
			}
	     
            if(bufferIndex != MAXFRAMESIZE-1) bufferIndex++;
            else bufferIndex = 0;

			if(bufferIndex > (byte)(bufferHead+5)){
				if(frame[(byte)(bufferHead+5)] == (byte)(bufferIndex-bufferHead-6)) {
					bufferHead = bufferIndex;
					puts("Head reset");
				}
			}		
	
            readyToReceive = 1;
			bitIndex=7;
        }
    
}
 
 
/* Funktionen für das Einrichten von Interrupts */

void setup_Timer()
{
    
    OCR1A = 12000000/256/100;   
    TCCR1B |= (1 << WGM12);
    
    // Timer/Counter Interrupt Mask Register - set on compare match
    TIMSK1 |= (1 << OCIE1A);
    
    //Set prescaler to 256
    TCCR1B |= (1 << CS12);
}

void setup_PinC()
{
    PCICR |= (1 << PCIE2); 

    PCIFR |= 0x7; 
    
    PCMSK2 |= (1 <<PCINT20);
}

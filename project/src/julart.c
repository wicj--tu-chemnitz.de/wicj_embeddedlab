#include "julart.h"
#include "lay3.h"

/** A receive buffer for USART input. */
unsigned char usart_rb[URB_SIZE];

/** An index indicator for receive buffer #usart_rb. */
unsigned char rbIndex;

int uart_putchar(char c, FILE *stream) 
{
    if (c == '\n') {
        uart_putchar('\r', stream);
    }
    loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register is empty */
    UDR0 = c;
    return 0;
}

int uart_getchar(FILE *stream)
{  
    loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
    int c = UDR0;
    if(c == '\r') c = '\n';
    return c;
}

static FILE uart_io = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);


void my_uart_init(void)
{
    uart_init();

    stdout = &uart_io;
    stdin  = &uart_io;
    
    rbIndex = 0;

    return;
}

void uart_init(void)
{
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif
    
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */
    UCSR0B |= (1 << RXCIE0);            /* Enable reciever interrupt*/
}

void jflush()
{
    while(getchar() != '\n') puts("Eingabe beenden mit Enter"); 
}

/** An interrupt service routine for USART input.
* When an interrupt is invoked, the incoming data is read from the receive register.
* The routine checks the input for correct input syntax (see diagram below).
* 
* \image html AcceptedInput.png
*
* In case of an accepted sequence of characters, the sequence is added to the message queue.
* 
* @param USART_RX_vect Enables USART interrupt. 
*/
ISR (USART_RX_vect)
{
    usart_rb[rbIndex] = UDR0;                     // Read data from the RX buffer   
   
    if(usart_rb[rbIndex] ==':'){
        char tmp = 0;
        for(int i = (int)rbIndex-1, j=1; i>=0 ; i--, j*=10){ //48 bis 57
            if(usart_rb[i] >= '0' && usart_rb[i]<= '9') tmp += (usart_rb[i]-'0') * j;
            else {
                puts("Für Adressen, bitte Ziffern von 0 bis 9 eingeben.");
                rbIndex = 0;
                return;
            }
        }
        usart_rb[0] = (unsigned char)tmp;
        rbIndex = 2;
    }
        
    else if(usart_rb[rbIndex] =='\n' || usart_rb[rbIndex] =='\r') {
        unsigned char* msg = malloc ((rbIndex)*sizeof(unsigned char));
        for(unsigned char i = 0; i < rbIndex; i++){
            msg[i]= usart_rb[i];
        }
        l3_usart_send(msg, rbIndex);
        free(msg);
        rbIndex = 0;
    }
    else {
        if(rbIndex>=URB_SIZE) {
            puts("Eingabe zu lang.");
            rbIndex=0;
        }
        else rbIndex++;
    }
}
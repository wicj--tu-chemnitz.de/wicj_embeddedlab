/*
 * A node has one next and one previous node. 
 * If a received frame has a different destination, then the node needs to relay the frame to the next node.
 * The node has a constant value as a unique node address. 
 * There may be broadcast messages (destination 0x00) on layer 3 -> It must be checked who was the sender of the frame to decide about the forwarding.
 *
 * Receiving 
 * =========
 * adress: const byte self = 205
 * 1. Check destination adress
 *		
 *		//if source == 0x00 : discard package
 * 		if destination == 0x00:
 *			if source == self : Broadcast successful, notify layer 4, discard package. 
 *			else relay message;
 * 		else if destination == self: hand package to layer4;
 *		else relay message;	
 * 		
 * No CRC-check at the intermediate-nodes between the sender and the receiver. 
 * If the destination differs from self, begin relaying the read Bytes to the next node, while still receiving the remaining ones.
 * -> An communication-error can be detected on the receivers end
 */

#include "lay3.h"

#include<stdio.h>
#include <util/atomic.h>

/** A variable indicating the head of the message queue #msgQueue. */
byte head = 0;

/** A variable indicating the tail of the message queue #msgQueue. */
byte tail = 0;

/** A function to copy the content of a frame into another ::byte-type array of the same length.
*/
void copyFrame(byte* from, byte* to, byte len)
{
    for(unsigned int i = 0; i < len; i++){
        to[i] = from[i];
    }
    return;
}

void broadcast_successful()
{
	puts("Broadcast successful.");
	return;
}

byte l3_action(byte dest, byte src)
{	
	/* Broadcast discovered. */
	if(dest == 0x00){
		if(src == SELF){
			return 0;
		}
	}

	/* A message sent by the node itself returned. 
	 * Thus, the receiver was not found in the ring. 
	 */ 
	else if(src == SELF) 
		// Discard message.
		return 3; 

	/* A message sent to the node. */
	else if(dest == SELF)
		// Receive the message and hand it to layer 4.
		return 1;

	/* A message not intended for the node to receive. Relay. */
	return 2;
}

void l3_exec(byte action, byte* frame)
{
	switch(action){

		/* Broadcast returns -> success. */
		case 0:
			broadcast_successful();
			break;

		/* Receive a message and hand it to layer 4*/
		case 1:
			puts("Handing package to layer 4.");
		    break;

		/* Received a message to relay.*/
		case 2:

			/* Check if queue is full
			 * Attention: Overflow possible!
			 * E.g. 8-byte Queue: index of tail is 7, index of head is 0 but 0-1 != 7. Still the queue is full.
			 */
				if(head == 0) head = MAXQUEUESIZE-2;
				else head--;
				
				ATOMIC_BLOCK(ATOMIC_FORCEON){
    				msgQueue[head] = realloc(msgQueue[head], (frame[5]+1) * sizeof(byte));
    				copyFrame(frame+5, msgQueue[head], frame[5]+1);
				}
			break;

		/* If message received, discard. Still send what's left in the queue. */
		default:
			l3_send(); 
			break;
	}
	return;
}

void l3_send(){
	if(head != tail) {
	    fillSend(msgQueue[head]+1, msgQueue[head][0]);
        
        if(head == MAXQUEUESIZE-2) head = 0;
        else head++;
	}
    return;
}

void l3_usart_send(byte* msg, byte len){
	puts("Adding own Message to queue ...");
	if(tail == head-1 || (head == 0 && tail == MAXQUEUESIZE-1)) puts("Message Queue full.");
    else{	    
        ATOMIC_BLOCK(ATOMIC_FORCEON){
            msgQueue[tail] = realloc(msgQueue[tail], (len+1) * sizeof(byte));
            msgQueue[tail][0] = len;
            copyFrame(msg, msgQueue[tail]+1, len);
            msgQueue[tail][2] = SELF;
            
            if(tail == MAXQUEUESIZE-2) tail = 0;
            else tail++;
        }
        
        /*
        //Possibility to print the content of the sent frame.
        for(int i = 0; i<msgQueue[tail-1][0]+1; i++)
		    printf("%u ", msgQueue[tail-1][i]);
	    puts("");
	   */
    }
}

/*
 * Priorities
 * ==========
 * Packages that need to be relayed should be handled with a higher priority than own sending-wishes of the node. 
 * Sending-wishes should be handled in order of arrival. This ensures, that own sending-wishes may not block the ring and congestions should not occur. 
 * If the node has no time to prepare its own sending-wishes, it cannot generate additional load onto the network. 
 * Also prioritizing packages from other nodes helps them holding their time-constraints.
 */



# Lab Embedded Software
## Implementation of the RaspNet protocol [1]
[TOC]
### Brief description
This project implements the simple communication protocol RaspNet between two or more Raspberry Pi computers - RaspNet. Therefor a Pi is extended with a Gertboard extension-board and uses its processor ATMEL Atmega328p (MCU) for the transmission and reception of message frames on a unidirectional ring infrastructure (see figure 1).
The protocol specifies the handling of basic reliability aspects as well as a simple routing function.  It also implements a broadcast mechanism.

<img src="project/doc/images/ring.png" width="25%" title="Architecture of the ring network with four nodes">  
Figure 1: Architecture of the ring network with four nodes [1]

### General architecture
RaspNet describes the communication between two or more nodes interconnected in a ring infrastructure. Sent frames can be of a maximum size of 256 bytes. A frame consists of a preamble (01111110), 4 bytes for the CRC value of the payload to transmit, 1 byte for the size of the payload whereas the following bytes for the payload include both the payload as well as the destination and the source adresses.

<img src="project/doc/images/lay2.png" width="35%" title="Layer 2 package">  
Figure 2: Basic structure of the layer 2 part of a RaspNet frame [1]  
<img src="project/doc/images/lay3.png" width="35%" title="Layer 3 package">  
Figure 3: Basic structure of the layer 3 part of a RaspNet frame [1]

The protocol consists of four layers:

* physical layer
* data-link layer
* network layer
* transport layer

At the moment only layers 1 to 3 are implemented in the project. Each one of the implemented layers will be decribed briefly in the following paragraphs.

### Makefile 
In this section the general composition of the Makefile is explained. Makefile commands should be called from within the project directory.

There are 7 functionalities the Makefile offers:

* `make all` (or simply `make`) compiles changed source files, generates the .hex file for the target (if not yet available) and flashes it to the MCU. In addition, it generates the documentation and opens its HTML version.
* `make flash` compiles changed source files, generates the .hex file for the target (if not yet available) and flashes it to the MCU.
* `make hexhex` compiles changed source files and generates the .hex file for the target (if not yet available).
* `make compile` compiles changed source files.
* As doc is a .PHONY target, whenever `make doc` is called, the documentation is newly generated.
* `make opendoc` generates the documentation and opens its HTML version.
* `make clean` removes all object files from the build-directory as well as all files created during the generation of the documentation except included images.

For functionalities 1 to 4 generated object files and .hex files are saved in the build-directory within the project's directory. All files created during the generation of the documentation (functionalities 5 and 6) are saved in the doc-directory within the project's directory.

### Performance
This implementation was successfully tested with the speed of sending and receiving 2000 bits per seconds. 

### References
[1] https://osg.informatik.tu-chemnitz.de/lehre/emblab/protocol.pdf

<!-- The detailed composition of a frame is decribed in subsections 2 and 3. --> 
<!---
#### 1.) Layer 1 - Physical Layer
The first layer is the physical layer. It specifies the timing behaviour of clock signal and data which are sent each via a wire. The wire is connected to a pin of the node's Gertboard on the one side and to the next node's Gertboard on the other side. When the clock signal changes, a new bit is available for reading on the data pin. The data signal may change between two clock changes, so there should be a phase shift between data and clock signal. The clock signal is completely independent from the data signal. Thus, it should continue to toggle even if there is no data to send.

#### 2.) Layer 2 - Data-link Layer
The second layer expands the funcitionality of layer 1 with simple reliability aspects. As mentioned earlier a frame is sized 256 bytes. 
--->


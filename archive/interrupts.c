#define F_CPU 12000000UL
#define BAUD 9600

#include<stdio.h>
#include<avr/io.h>
#include<util/delay.h>
#include<util/setbaud.h>
#include<avr/interrupt.h>
#include"julart.h"

#define _NOP()


/* 
* Eigene Funktion, die die dynamische Eingabe einer Zeitspanne zum Warten ermöglicht. 
* Nötig, da _delay() seine Parameter zur Compile-Zeit verlangt.
*/
void delay(int ms){
    while(ms>0){
	_delay_ms(1);
	ms--;
    }
    return;
}

int main(void){
    uart_init();
    //Initialisierung Input-/Output-Stream
    stdout = &uart_io;
    stdin  = &uart_io;
    
    //Data Direction
    DDRD = 0x0;
    //PORTD = 0x10;
    DDRB = 0x38; //(00111000) --> output at B3, B4, B5
    
 /* Timer Interrupt  
    /* Output Compare Register
     * --> highest value can be FFFF because we have 16 bit available
     * 
     * OCRn =  [ (clock_speed / Prescaler_value) * Desired_time_in_Seconds ] - 1
     *      = 12000000/256 * 1 - 1    
     *      = 46875(DEC)    
     *      = B71A(HEX) 
     *
    OCR1A = 0xB71A;
    
 
    /* enable CTC-mode (Clear Timer on Compare) 
     * Each clock cycle TCTn register is incremented by 1. 
     * If TCNTn == OCR1A a timer overflow occurs which can trigger an interrupt.
     *
    TCCR1B |= (1 << WGM12);
    
    // Timer/Counter Interrupt Mask Register - set on compare match
    TIMSK1 |= (1 << OCIE1A);
    
    //Set prescaler to 256
    TCCR1B |= (1 << CS12);
*/

/* Pin Change Interrupt
 * 
 */
	PCICR |= (1<<PCIE2); // Enable checking PCMSK2
	PCIFR |= 0x7; // Clear Pin Change Interrupt Flag
	PCMSK2 |= (1 <<PCINT20); //Pin Change Interrupt enabled for Pin D4
    
    //setup interrupt -- set SREG (one)
    sei();
    
    while(1){
		delay(500);
		PORTB ^= 16;
	}  
    
    
    return 0;
}

/*
 * Interrupt Service Routine using 16-Bit Timer
 *
 ISR(TIMER1_COMPA_vect){
	PORTB ^= 16;
	
	_NOP(); // for synchronization

	if(PIND & (1<<PD4)) {
		//Daten kommen an
		puts("1");
		PORTB ^= 8;
	}
	else{
		//Es kommen keine Daten an.
		puts("0");
		PORTB ^= 8;
	}
	//Ich habe bei Nutzung dieser ISR unter Änderung der Frequnz keine besonderen Ereignisse beobachtet.
}
*/

/* 
 * Interrupt Service Routine using Pin Change
 * PCINT2 covers PCINT[16...23]
 * PCINT20 -- PD4 
 */
 
 ISR(PCINT2_vect){	
	//_NOP(); // for synchronization

	if(PIND & (1<<PD4)) {
		//Daten kommen an
		puts("1");
		PORTB ^= 8;
	}
	else{
		//Es kommen keine Daten an.
		puts("0");
		PORTB ^= 8;
	}
 
 }

/*
* Quellen:
* ========
* [1] https://appelsiini.net/2011/simple-usart-with-avr-libc/
*/

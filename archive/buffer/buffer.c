#define F_CPU 12000000UL
#define BAUD 9600

#define ATOMIC_BLOCK(type)
#define ATOMIC_RESTORESTATE
#define ATOMIC_FORCEON

#include<stdio.h>
#include<avr/io.h>
#include<util/delay.h>
#include<util/setbaud.h>
#include<avr/interrupt.h>
#include<util/atomic.h>
#include"julart.h"

typedef unsigned char byte;

#define MAXFRAMESIZE 2

// Init of Send-/Receive-Buffer
const byte send = 0xB3; // 10110011
byte frame[MAXFRAMESIZE]; // 0 - Preambel, 1 to 4 - CRC checksum, 5 - size, 6 to n (n = max. ) - Payload

byte flag = 0;
byte bufferIndex = 0; // Zählvariable für ISR
byte receiveIndex = 0;
byte bitIndex = 0;

//frame = {0b01111110, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b10110011};

void delay(int ms){
    while(ms>0){
	_delay_ms(1);
	ms--;
    }
    return;
}

/* Funktionsdeklarationen für Interruptsetups */
void setup_Timer();
void setup_PinC();

int main(void){
    uart_init();
    //Init Input-/Output-Stream
    stdout = &uart_io;
    stdin  = &uart_io;
    
    //Data Direction
    DDRD = 0x0;
    //PORTD = 0x10;
    DDRB = 0x38; //(00111000) --> output at B3, B4, B5
	
    /* Setup Interrupt  */
    setup_Timer();
    setup_PinC();
    sei(); //set SREG (one)
    
    
    byte receivedFrame[MAXFRAMESIZE];
    receivedFrame[0] = 0;
    while(1){
        ATOMIC_BLOCK(ATOMIC_FORCEON){
            if(bitIndex==0){
                if(receiveIndex<bufferIndex){
                    receivedFrame[receiveIndex] = frame[bufferIndex-1];
                
                }
                printf("Empfangen: %d\n", receivedFrame[receiveIndex]);
            }
        }
    }  
    return 0;
}

/*
 * Interrupt Service Routine using 16-Bit Timer
 */
 
 ISR(TIMER1_COMPA_vect){
    //Wenn flag gesetzt, sende Daten.
    if(flag){
        // i-te Ziffer vom Sendebuffer wird extrahiert
        byte bit = (send & (1 << bitIndex)) >> bitIndex;    
        if(bit) {
            PORTB |= (1 << PB5);  //PB5 im PORTB setzen ()
            PORTB |= (1 << PB3);   //PB3 setzen -> LED an
        }
        else {
            PORTB &= ~(1 << PB5); //PB5 im PORTB loeschen
            PORTB &= ~(1 << PB3); //PB3 löschen -> LED aus
        }
    }
    else    PORTB ^= (1 << PB4); //Clock Signal -> Pin Change Interrupt
    flag^=1;
}

/* 
 * Interrupt Service Routine using Pin Change
 * PCINT2 covers PCINT[16...23]
 * PCINT20 -- PD4 
 */
 
 ISR(PCINT2_vect){

    frame[bufferIndex] = (frame[bufferIndex] << 1) + ((PIND & (1 << PD5)) >> PD5);
    bitIndex++;
    if(bitIndex == 8) {
        printf("Gesendet: %d\n", frame[bufferIndex]);
        if(bufferIndex != MAXFRAMESIZE-1) bufferIndex++;
        bitIndex=0;
    }
    //received |= (PIND & (1 << PD5)) << bitIndex;
    //received = (received << 1) + ((PIND & ( 1 << PD5)) >> PD5);
    
 }
 
 
/* Funktionen für das Einrichten von Interrupts */
/* Timer Interrupt */ 
void setup_Timer(){
    /* Output Compare Register
     * --> highest value can be FFFF because we have 16 bit available
     * 
     * OCRn =  [ (clock_speed / Prescaler_value) * Desired_time_in_Seconds ] - 1
     *      = 12000000/256 * 0.5 - 1    
     *      = 23436 (DEC)    
     *      = 5B8C(HEX)
    */
    OCR1A = 0x5B8C;
    
    // If TCNTn == OCR1A a timer overflow occurs which can trigger an interrupt.
    TCCR1B |= (1 << WGM12);
    
    // Timer/Counter Interrupt Mask Register - set on compare match
    TIMSK1 |= (1 << OCIE1A);
    
    //Set prescaler to 256
    TCCR1B |= (1 << CS12);
}

/* Pin Change Interrupt */
void setup_PinC(){
    // Enable checking PCMSK2
    PCICR |= (1 << PCIE2); 
    
	// Clear Pin Change Interrupt Flag
	PCIFR |= 0x7; 
	
	//Pin Change Interrupt enabled for Pin D4
	PCMSK2 |= (1 <<PCINT20);
}

/*
* Quellen:
* ========
* [1] https://appelsiini.net/2011/simple-usart-with-avr-libc/
*/

#define F_CPU 12000000UL
#define BAUD 9600

#define 	ATOMIC_BLOCK(type)
#define 	ATOMIC_RESTORESTATE
#define 	ATOMIC_FORCEON

#include<stdio.h>
#include<avr/io.h>
#include<util/delay.h>
#include<util/setbaud.h>
#include<avr/interrupt.h>
#include<util/atomic.h>
#include"julart.h"

// Init of Send-"Buffer"
const unsigned char send = 0xB3; // 10110011
char isrc=7; // Zählvariable für ISR

void delay(int ms){
    while(ms>0){
	_delay_ms(1);
	ms--;
    }
    return;
}

void to_binary(unsigned char val){
    int i = 0; 
    char bits[8] = {0,0,0,0,0,0,0,0};
    while (val > 0) { 
        // storing remainder in bit array 
        bits[i] = val % 2;
        val = val / 2; 
        i++; 
    } 
    for(int i =0; i<8; i++) printf("%d ", bits[i]);
    printf("\n");
    return;
}

/* Funktionsdeklarationen für Interruptsetups */
void setup_Timer();
void setup_PinC();

int main(void){
    uart_init();
    //Init Input-/Output-Stream
    stdout = &uart_io;
    stdin  = &uart_io;
    
    //Data Direction
    DDRD = 0x0;
    //PORTD = 0x10;
    DDRB = 0x38; //(00111000) --> output at B3, B4, B5
//    PORTB ^= (1<<PB5);
	
    /* Setup Interrupt  */
    setup_Timer();
    setup_PinC();
    sei(); //set SREG (one)
    
    //Init Receive-Buffer
    unsigned char received = 0;
    delay(500);
    while(1){
        
        ATOMIC_BLOCK(ATOMIC_FORCEON){
            //received |= (PIND & (1 << PD5)) << isrc;
            
            received = (received << 1) + ((PIND & ( 1 << PD5)) >> PD5);
            
            //printf("%d\n", received);
            
            if(isrc==-1) {
                printf("Ich habe folgendes empfangen: ");
                to_binary(received);
                received = 0;
                isrc = 7;
            }
        }
        delay(1000);
    }  
    return 0;
}

/*
 * Interrupt Service Routine using 16-Bit Timer
 */
 
 
 
 ISR(TIMER1_COMPA_vect){
    PORTB ^= (1 << PB4);
}

/* 
 * Interrupt Service Routine using Pin Change
 * PCINT0 covers PCINT[0...7]
 * PCINT3 -- PB3 
 */

// ISR(PCINT0_vect){}

/* 
 * Interrupt Service Routine using Pin Change
 * PCINT2 covers PCINT[16...23]
 * PCINT20 -- PD4 
 */
 
 ISR(PCINT2_vect){
    
    //if (isrc==8) isrc = 0; //Beginne von vorn
    //printf("isrc: %d\n", isrc);
    unsigned char bit = (send & (1 << isrc)) >> isrc;    // i-te Ziffer von send wird extrahiert
    //printf("bit: %d\n", bit);
    if(bit>0){ 
        if((PIND & (1 << PD5))) {
	    printf("bit = 1; PIN5 = 1\n"); //falls an PD5 schon 1 anliegt, muss nichts geändert werden.
	    //PORTB &= ~(1 << PB0); //PB5 im PORTB loeschen
	}
	else {
		PORTB |= (1 << PB5);  //PB0 im PORTB setzen
	        PORTB |= (1<<PB3);   //falls 0 an PD5 liegt, Strom an -> Setze PB3.
	        printf("bit = 1; PIN5 = 0\n");
	}
    }
    else {
        if((PIND & (1 << PD5))) {
            PORTB &= ~(1 << PB5); //PB5 im PORTB loeschen
	    PORTB &= ~(1 << PB3);
	    //PORTB ^= (1<<PB5); //falls PD5 = 1, aber es soll gar nichts gesendet werden, Strom aus -> Reset PB4.
            printf("bit = 0; PIN5 = 1\n");
        }
        else {
		printf("bit = 0; PIN5 = 0\n");
		//PORTB |= (1 << PB5);  //PB5 im PORTB setzen
        }
    }
    isrc--;
    
 }
 
 
/* Funktionen für das Einrichten von Interrupts */
/* Timer Interrupt */ 
void setup_Timer(){
    /* Output Compare Register */
    OCR1A = 0xB71A;
    
    // If TCNTn == OCR1A a timer overflow occurs which can trigger an interrupt.
    TCCR1B |= (1 << WGM12);
    
    // Timer/Counter Interrupt Mask Register - set on compare match
    TIMSK1 |= (1 << OCIE1A);
    
    //Set prescaler to 256
    TCCR1B |= (1 << CS12);
}

/* Pin Change Interrupt */
void setup_PinC(){
    // Enable checking PCMSK2 (& PCMSK0)
    PCICR |= (1 << PCIE2); //| (1 << PCIE0);
    
	// Clear Pin Change Interrupt Flag
	PCIFR |= 0x7; 
	
	//Pin Change Interrupt enabled for Pin D4 (& B3)
	PCMSK2 |= (1 <<PCINT20);
	//PCMSK0 |= (1 << PCINT3);
}

/*
* Quellen:
* ========
* [1] https://appelsiini.net/2011/simple-usart-with-avr-libc/
*/

#define F_CPU 12000000UL

#include<util/delay.h>
#include<avr/io.h>

int main(void){
    DDRD = 0b11111100;
    DDRB = 0b00111111;
    PORTD = 0b00000011;
    PORTB = 0b00000000; 
    char on = 0;
    while(1){
		while(!on){
			if(PORTD != 0b11111111) PORTD = (PORTD<<1)+1;
			else{
				if(PORTB != 0b11111111) PORTB = (PORTB<<1)+1;
				else on=1;
			}
			_delay_ms(250);
		}
		while(on){
			if(PORTB != 0b00000000) PORTB = (PORTB-1)>>1;
			else{
				if(PORTD != 0b00000011) PORTD = (PORTD-1)>>1;
				else on=0;
			}
			_delay_ms(250);
		}
    }
}

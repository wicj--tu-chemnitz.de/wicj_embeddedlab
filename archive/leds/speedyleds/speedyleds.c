#define F_CPU 12000000UL
#define BAUD 9600

#include<stdio.h>
#include<stdlib.h>
#include<avr/io.h>
#include<util/delay.h>
#include<util/setbaud.h>

/* 
* uart_init() komplett aus [1]
* *_putchar(), *_getchar() größtenteils aus [1] 
*/

void uart_init(void){
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif
    
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */
}

void uart_putchar(char c, FILE *stream) {
    if (c == '\n') {
        uart_putchar('\r', stream);
    }
    loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register is empty */
    UDR0 = c;
}

char uart_getchar(FILE *stream) {
    loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
    char c = UDR0;
    if(c == '\r') c = '\n';
    return c;
}

void jflush(){
    while(getchar() != '\n') puts("Eingabe beenden mit Enter"); 
}

FILE uart_io = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);
   

/* 
* Eigene Delay Funktion, die dynamische Eingabe von Zeitspanne erlaubt. 
* Nötig, da _delay() Parameter zur Compile-Zeit verlangt 
*/
void delay(int ms){
    while(ms>0){
	_delay_ms(1);
	ms--;
    }
    return;
}

void change(int * input){
    char c; // zur Abfrage ob Änderungswunsch besteht
    puts("Want to change speed? (y/n)");
    //Eingabe y/n
    c = getchar();
    if(c=='n')	puts("Ok.");
    else if(c=='y'){
	puts("What do you want to change it to?");
	//Eingabe neue Geschwindigkeit
	scanf("%d", input);
	jflush();
	printf("%d\n", *input);
    }
    else {
	//Bei fehlerhafter Eingabe
	puts("Please choose either y or n.");
	change(input);
    }
    return;
}

/*
void debug(int d, int b){
    char str[33];
    int c = 0;
    c=d<<8;
    c |= b;
    itoa(c,str,2);
    printf("%s\n", str);
    return;
}
*/

void debug(int d, int b){
    int c = 0;
    c=b<<8;
    c |= d;
    for(int i = 2; i < 14; i++){
	printf("%d", (c & (1 << i)) !=0);
    }
    puts("");
    return;
}

int main(void){
    uart_init();
    //Initialisierung Input-/Output-Stream
    stdout = &uart_io;
    stdin  = &uart_io;
    
    //Initialisierung Data Direction Register
    DDRD = 0b11111100;
    DDRB = 0b00111111;
    //Initialisierung Portbelegung
    PORTD = 0b00000011;
    PORTB = 0b00000000; 
    //Initialisierung Hilfsvariablen
    char on = 0; // 0, wenn alle LEDs aus ; 1, wenn alle LEDs an
    int input; //Speicherung der Eingabe für Geschwindigkeitsanpassung

    puts("Input speed: ");
    scanf("%d", &input);
    jflush();
    printf("%d\n", input);    
    
    while(1){
	while(!on){
	    debug(PORTD, PORTB);
	    //bitweise Linksshift und +1 zum Einschalten der einzelnen LEDs (D-Ports)
		if(PORTD != 0b11111111) PORTD = (PORTD<<1)+1;
		//wenn alle LEDs an D-Ports an:
		else{
			//bitweise Linksshift und +1 zum Einschalten der einzelnen LEDs (B-Ports)
			if(PORTB != 0b11111111) PORTB = (PORTB<<1)+1;
			//wenn alle LEDs an:
			else{
			    on=1;
			}
		}
		delay(input);
	}
	while(on){
	    debug(PORTD, PORTB);
	    //bitweise -1 und Rechtsshift zum Einschalten der einzelnen LEDs (B-Ports)
		if(PORTB != 0b00000000) PORTB = (PORTB-1)>>1;
		//wenn alle LEDs an B-Ports aus:
		else{
		    //bitweise -1 und Rechtsshift zum Einschalten der einzelnen LEDs (D-Ports)
			if(PORTD != 0b00000011) PORTD = (PORTD-1)>>1;
			//wenn alle LEDs aus:
			else {
			    on=0;
			    //Frage, ob Geschwindigkeit geändert werden soll
			    change(&input);
			}
		}
		delay(input);
	}
    }
    
    return 0;
}

/*
* Quellen:
* ========
* [1] https://appelsiini.net/2011/simple-usart-with-avr-libc/
*/

/*
* Eingabe über minicom
* ====================
* Um die Eingabe über minicom zu erleichtern, gehe in die Einstellungen von minicom (CTRL-A Z). 
* Wähle configure Minicom (Buchstabe O). Wähle dann Serial Port Setup.
* Suche die Einstellungsmöglichkeiten "Hardware Flow Control" und "Software Flow Control" und setze beide auf No, wenn nicht bereits so gesetzt.
*/

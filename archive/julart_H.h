#ifndef JULART_H
#define JULART_H

void uart_init(void);
void uart_putchar(char c, FILE *stream);
char uart_getchar(FILE *stream);
void jflush();

#endif
#include "crc.h"

/* Implementing CRC as an Left-Shift-Register
 * Input not reflected, Result not reflected, Initial Value = 0, Final XOR Value = 0
 */
uint32_t crc_comp (byte* input, byte size){ //  brauche nur unsigned char weil size maximal 255 byte ist.
    //byte crc_key [5] = {1, 0b00000100, 0b11000001, 0b00011101, 0b10110111}; //0x104C11DB7
    uint32_t crc_key = 0x04C11DB7;
    
    // Init CRC register either with 0 or directly with first 32 bits of input
    uint32_t crc_reg = 0;

    // As long as MSB == 0 leftshift input into register
    // if MSB == 1 pop (bcs register is only 32bit big but key is 33bits). then XOR with crc_key
    // repeat until input is fully processed
    
    byte input_index = 0;
    
    while(input_index < size){
      	
      	crc_reg ^= ((uint32_t)input[input_index] << 24); 
    	
    	for(byte bit_index = 0; bit_index < 8; bit_index++){

	        //byte msb = (crc_reg[reg_index] & (1 << 7)) >> 7;
            uint32_t msb = (crc_reg & ((uint32_t)1 << 31)) >> 31;
	        
	        /*
	         * Left-shift current content of each byte of crc_reg by 1 position, 
	         * XOR with crc_key if msb == 1
	         */
            crc_reg <<= 1;
	        if(msb == 1) crc_reg = crc_reg ^ crc_key;
	    }
    	input_index++;
	}
    return crc_reg;
}

byte crc_check(byte* input){
    uint32_t checksum = 0;
    for(byte i = 1; i < 5; i++){
         checksum = checksum | ((uint32_t)input[i] << (32 - i*8));
    }

    byte* payload = malloc (input[5]*sizeof(byte));
    for(uint i = 6; i < (uint)input[5]+6; i++){
        payload[i-6] = input[i];
    }
    
    if(checksum == crc_comp(payload, input[5])) {
        free(payload);
        return 1;
    }
    else {
        free(payload);
        return 0;
    }
}

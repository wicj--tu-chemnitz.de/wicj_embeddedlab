#include <stdint.h>
#include <stdlib.h>

typedef unsigned int uint;
typedef unsigned char byte;
uint32_t crc_comp (byte* input, byte size);
byte crc_check(byte* input);

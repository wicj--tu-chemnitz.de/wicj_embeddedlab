#define F_CPU 12000000UL
#define BAUD 9600

#include <avr/io.h>
#include <stdio.h>
#include <util/setbaud.h>

#include <stdlib.h>
#include <stdint.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>

#include "julart.h" // Uart Functions
#include "crc.h" //crc implementation
#include "lay3.h"

//typedef unsigned char byte;

#define MAXFRAMESIZE 256 //in byte


// Init of Send-/Receive-Buffer
byte send[MAXFRAMESIZE]= {0}; // 0xB3, 0x10 = 10110011, 00010000
byte frame[MAXFRAMESIZE]; // 0 - Preambel, 1 to 4 - CRC checksum, 5 - size, 6 to n (n = max. 256) - Payload

byte flag = 1;
byte bufferIndex = 0; // Zählvariable für ISR
byte sendIndex = 0;
byte bitIndex = 7;
byte readyToReceive = 0;
byte readyToSend = 0;

//frame = {0b01111110, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b10110011};

void delay(int ms){
    while(ms>0){
    _delay_ms(1);
    ms--;
    }
    return;
}

void fillSend (byte* payload, byte size){
    //payload padding (Preambel, crc berechnung, size, rückgabe, readyToSend)

    send[0] = 0x7E; //01111110

    uint32_t checksum = crc_comp(payload, size);
    for(byte i = 1; i < 5; i++){
        byte x = 32-i*8;
        send[i] = (byte)((checksum & ((uint32_t)(255) << (x))) >> x);
    }

    send[5] = size;

    for(byte i = 6; i < size+6; i++){ //+6 für preamble, crc, size, 
        send[i]= payload[i-6];
    }
    readyToSend = 1;  
}


/* Funktionsdeklarationen für Interruptsetups */
void setup_Timer();
void setup_PinC();

int main(void){
    uart_init();
    //Init Input-/Output-Stream
    stdout = &uart_io;
    stdin  = &uart_io;
    
    //Data Direction
    DDRD = 0x0;
    //PORTD = 0x10;
    DDRB = 0x38; //(00111000) --> output at B3, B4, B5
    
    /* Setup Interrupt  */
    setup_Timer();
    setup_PinC();
    sei(); //set SREG (one)
    
    byte receiveIndex = 0;
    byte* receivedFrame = malloc(MAXFRAMESIZE*sizeof(byte));
    uint16_t sizeToReceive = MAXFRAMESIZE;
    byte action; // 0 - broadcast successful, to layer 4; 1 - receive, to layer 4; 2 - relay

    puts("Getting ready to send.");
            byte sizeToSend = 4;
            byte* payload = malloc((sizeToSend)*sizeof(byte));
            payload[0] = 211; //dest
            payload[1] = SELF; //src
            payload[2] = 0xB3;
            payload[3] = 0x10;

            ATOMIC_BLOCK(ATOMIC_FORCEON){
                fillSend(payload, sizeToSend);
                puts("Sendbuffer filled.");
            }
    
    while(1){
        //if(!readyToSend){}
        ATOMIC_BLOCK(ATOMIC_FORCEON){
            // Data Package: 0 Preamble, 1 - 4 CRC,  5 Length,  6 Destination, 7 Source, 8 - n Data
            if(readyToReceive){
                if(bufferIndex != 0) receivedFrame[receiveIndex] = frame[bufferIndex-1];
                else receivedFrame[receiveIndex] = frame[MAXFRAMESIZE-1];
                //printf("receivedFrame[%d] = %d, bufferIndex: %d \n", receiveIndex, receivedFrame[receiveIndex], bufferIndex);
                
                if(receiveIndex==0){
                        if(receivedFrame[receiveIndex] == 0x7E) receiveIndex++;
                }
                else if(receiveIndex==5){
                        sizeToReceive = (uint16_t)receivedFrame[receiveIndex];
                        //receivedFrame = (byte*) realloc (receivedFrame, (sizeToReceive+6)*sizeof(byte));
                        receiveIndex++;
                }
                else if(receiveIndex==sizeToReceive+5){
                            if(!crc_check(receivedFrame)){
                                puts("CRC-Check failed");
                                for(uint i=0; i < sizeToReceive+6; i++)
                                    printf("%d ", receivedFrame[i] );
                            }
                            else{
                                puts("CRC-Check successful.\nReceived: ");
                                for(uint i=0; i < sizeToReceive+6; i++)
                                    printf("%d ", receivedFrame[i] );
                                printf("\n");
                                //Hier evtl. noch Speicherung und/oder Ausgabe einbauen.
                            }
                            l3_exec(action, receivedFrame);
                            receiveIndex = 0;
                }
                else if(receiveIndex==7){ //check destination
                    action = l3_action(receivedFrame[6], receivedFrame[7]);
                    receiveIndex++;
                }
                else{
                    receiveIndex++;
                }
                
                readyToReceive = 0;
                
                
            }
        }
    }  
    return 0;
}

/*
 * Interrupt Service Routine using 16-Bit Timer
 */
 
 ISR(TIMER1_COMPA_vect){
     //Wenn flag gesetzt, sende Daten.
    if(flag){
        if(readyToSend){
            // i-te Ziffer vom Sendebuffer wird extrahiert
            byte bit = (send[sendIndex] & (1 << bitIndex)) >> bitIndex;    
            if(bit) {
                PORTB |= (1 << PB5);  //PB5 im PORTB setzen ()
                PORTB |= (1 << PB3);   //PB3 setzen -> LED an
            }
            else {
                PORTB &= ~(1 << PB5); //PB5 im PORTB loeschen
                PORTB &= ~(1 << PB3); //PB3 löschen -> LED aus
            }
            if(bitIndex==0) sendIndex++;
            if(sendIndex == (uint16_t)send[5]+6) {
                sendIndex = 0;
                readyToSend = 0;
            }
        }
    }
    else    PORTB ^= (1 << PB4); //Clock Signal -> Pin Change Interrupt
    flag^=1;
}

/* 
 * Interrupt Service Routine using Pin Change
 * PCINT2 covers PCINT[16...23]
 * PCINT20 -- PD4 
 */
 
 ISR(PCINT2_vect){
    
        frame[bufferIndex] = (frame[bufferIndex] << 1) + ((PIND & (1 << PD5)) >> PD5);
        bitIndex--;
        if(bitIndex == 255) {
            if(bufferIndex != MAXFRAMESIZE-1) bufferIndex++;
            else bufferIndex = 0;
            readyToReceive = 1;
            bitIndex=7;
        }
        //frame[bufferIndex] = (frame[bufferIndex] << 1) + ((PIND & (1 << PD5)) >> PD5);
        //received |= (PIND & (1 << PD5)) << bitIndex;
        //received = (received << 1) + ((PIND & ( 1 << PD5)) >> PD5);
    
}
 
 
/* Funktionen für das Einrichten von Interrupts */
/* Timer Interrupt */ 
void setup_Timer(){
    /* Output Compare Register
     * --> highest value can be FFFF because we have 16 bit available
     * 
     * OCRn =  [ (clock_speed / Prescaler_value) * Desired_time_in_Seconds ] - 1
     *      = 12000000/256 * 0.5 - 1    
     *      = 23436 (DEC)    
     *      = 5B8C(HEX)
    */
    OCR1A = 0x1D4; // = 0.01s //0x124F; // = 0.1s    
    // If TCNTn == OCR1A a timer overflow occurs which can trigger an interrupt.
    TCCR1B |= (1 << WGM12);
    
    // Timer/Counter Interrupt Mask Register - set on compare match
    TIMSK1 |= (1 << OCIE1A);
    
    //Set prescaler to 256
    TCCR1B |= (1 << CS12);
}

/* Pin Change Interrupt */
void setup_PinC(){
    // Enable checking PCMSK2 
    PCICR |= (1 << PCIE2); 

    // Clear Pin Change Interrupt Flag
    PCIFR |= 0x7; 
    
    //Pin Change Interrupt enabled for Pin D4
    PCMSK2 |= (1 <<PCINT20);
}

/*
* Quellen:
* ========
* [1] https://appelsiini.net/2011/simple-usart-with-avr-libc/
*/

/*
 * What happens in your implementation when the receiver of a message cannot be found in the network? 
 * What should happen? How do you realize the demanded priorization scheme?
 *
 *
 * A node has one next and one previous node. 
 * If a received frame has a different destination, then the node needs to relay the frame to the next node.
 * The unique node addresses should be a constant value (number of your Pi board as default value) 
 * There may be broadcast messages (destination 0x00) on layer 3 -> It must be checked who was the sender of the frame to decide about the forwarding.
 *
 * Receiving 
 * =========
 * adress: const byte self = 205
 * 1. Check destination adress
 *		
 *		//if source == 0x00 : discard package
 * 		if destination == 0x00:
 			if source == self : Broadcast successful, notify layer 4, discard package. 
 			else relay message;
 * 		else if destination == self: hand package to layer4;
 *		else relay message;
 		
 * 		
 *		
 *
 * No CRC-check at the intermediate-nodes between the sender and the receiver. 
 * If the destination differs from self, begin relaying the read Bytes to the next node, while still receiving the remaining ones.
 * -> An communication-error can be detected on the receivers end
 */

#include "crc.h"
#include "lay3.h"

#include<stdio.h>

void copyFrame(byte* from, byte* to){
    for(int i = 0; i < sizeof(to) && i < sizeof(from); i++){
        to[i] = from[i];
    }
    return;
}

void broadcast_successful(){
	puts("Broadcast successful.\n");
	return;
}

byte l3_action(byte dest, byte src){
	if(dest == 0x00){
		if(src == SELF){
			return 0;
		}
	}
	else if(dest == SELF)
		return 1; //receive, to layer 4
	return 2; //relay
}

void l3_exec(byte action, byte* frame){
	puts("l3_exec: ");
	switch(action){
		case 0:
			broadcast_successful();
			break;
		case 1: //receive
			puts("Handing package to layer 4.\n");
		    break;
		case 2: //relay
			//fillSend(frame+6, frame[5]);
			queue = realloc(queue, tail+1);
			queue[tail] = realloc(queue[tail], sizeof(frame));
			copyFrame(frame, queue[tail]);
			tail++;
			
			//evtl. state = 2 setzen für main -> weiß, dass jetzt priorisiert weitergeleitet werden muss
			
			// hier noch head merken und/oder Index zum senden anpassen. -> Priority
			break;
		default:
		break;
	}
	return;
}


/* Ich muss noch die Funktion meines Sendebuffers erweitern. Ich möchte byteweise senden können, damit ich relayen kann, ohne viel zwischenzuspeichern.
 *
 */

/*
 * Priorities
 * ==========
 * Packages that need to be relayed should be handled with a higher priority than own sending-wishes of the node. 
 * Sending-wishes should be handled in order of arrival. This ensures, that own sending-wishes may not block the ring and congestions should not occur. 
 * If the node has no time to prepare its own sending-wishes, it cannot generate additional load onto the network. 
 * Also prioritizing packages from other nodes helps them holding their time-constraints.
 */


